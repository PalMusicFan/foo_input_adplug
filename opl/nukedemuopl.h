/*
 * Adplug - Replayer for many OPL2/OPL3 audio file formats.
 * Copyright (C) 1999 - 2005 Simon Peter, <dn.tlp@gmx.net>, et al.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * akemuopl.h - Emulated OPL using Alexey Khokholov's emulator,
 *              by Chris Moeller <kode54@gmail.com>
 */

#ifndef H_ADPLUG_NUKEDEMUOPL
#define H_ADPLUG_NUKEDEMUOPL

#include <opl.h>
#include <stdlib.h>
#include "opl.h"

#define clip(s) if ( (s + 0x8000) & 0xFFFF0000 ) (s) = (((s) >> 31) ^ 0x7FFF)

class Nukedemuopl: public Copl
{
public:
	Nukedemuopl(int rate, bool stereo)
		: usestereo( stereo )
	{
		opl = NukedOPL3Create(false);
		currType = TYPE_OPL3;
	};

	~Nukedemuopl()
	{
		delete opl;
	}

	void update(short *buf, int total)
	{
		float buffer[256 * 2];
		while (total > 0)
		{
			int samples = total;
			if ( samples > 256 ) samples = 256;
			{
				memset(buffer, 0, samples * sizeof(float) * 2);
				opl->Update(buffer, samples);
				if ( usestereo )
				{
					for ( unsigned i = 0, j = samples * 2; i < j; i++ )
					{
						int sample = buffer [i] * 32768.0f;
						clip(sample);
						buf [i] = sample;
					}
				}
				else
				{
					for ( unsigned i = 0; i < samples; i++ )
					{
						int sample = ( buffer [i * 2 + 0] + buffer [i * 2 + 1] ) * 32768.0f / 2.0f;
						clip(sample);
						buf [i] = sample;
					}
				}
			}
			total -= samples;
			if ( usestereo ) buf += samples * 2;
			else buf += samples;
		}
	}

	// template methods
	void write(int reg, int val)
	{
		opl->WriteReg(currChip * 0x100 + reg, val);
	};

	void init() {};

	void settype(ChipType type)
	{
		currType = type;
	}

private:
	bool usestereo;
	OPLEmul * opl;
};

#endif
